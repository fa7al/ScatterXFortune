/**
 * Created by satya on 24/6/18.
 */

// to upload a file to s3 which we are adding to textboxio

function get_signed_url(delegation_id, file_data, attachment_type) {
    var file_div = $('.fileuploader-items-list').find('[title="'+file_data.name+'"]');
    var file_name = file_data.name;
    if (file_div.length) {
        $(file_div).addClass('attachment-uploading');
        //Add a timestamp to the file name
        var file_name_array = file_name.split('.');
        if (file_name_array.length>1) {
            var file_type = file_name_array.pop();
            file_name = file_name_array.join() +'_' + new Date().getTime() +'.'+ file_type;
        }
    }
    $.ajax({
        url: '/inhouse/fortune_upload_to_s3',
        data: {
            "file_name": file_name,
            "file_type": file_data.type,
            'del_id': delegation_id,
            'attachment_type': attachment_type
        },
        method: 'POST',
        success: function (response) {
            uploadFile(file_data, response.data, response.url, response.upload_url);
        },
        error: function (response) {//added error function for event attachment
            if (file_div.length) {
                $(file_div).removeClass('attachment-uploading');
                if (!$('#upload_div_'+delegation_id).find('.attachment-uploading').length) {
                    $('#upload_all_'+delegation_id).removeClass('disabled');
                    $.fileuploader.getInstance($("#event_attachment_"+delegation_id)).enable();
                }
            }
            $('#message_modal .modal-body').html('Could not upload attachment.Please check your internet connection!');
            $('#message_modal').modal('show');
        },
    });
}
function uploadFile(file, s3Data, url, upload_url) {
    var xhr = new XMLHttpRequest();
    var file_div = $('.fileuploader-items-list').find('[title="'+file.name+'"]')[0];
    var next_span = $(file_div).next();
    xhr.upload.addEventListener('progress', function(e) {
        var uploaded= parseInt(e.loaded/e.total*100)+"%";
        next_span.text(uploaded);
        }, false);
    xhr.open("POST", upload_url);
    var key_tuples = [['key','key'],['acl','acl'],['Content-Type','Content_Type'],
        ['X-Amz-Credential','x_amz_credential'],['X-Amz-Algorithm','x_amz_algorithm'],['X-Amz-Date','x_amz_date'],
        ['Policy','policy'],['X-Amz-Signature','x_amz_signature'],];
    var postData = new FormData();
    for (var i=0;i<key_tuples.length;i++) {
        postData.append(key_tuples[i][0], s3Data[key_tuples[i][1]]);
    }
    postData.append('file', file);
    var delegation_id = $(".nav-tabs li.tab-press.active").find('a').attr('href').match(/\d+$/)[0];
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            var text = $(file_div).text();
            if (xhr.status === 200 || xhr.status === 204) {
                var comment_text = "Attached a new file &nbsp;"+"<a target='_blank' href='"+url+"'>"+file.name+"</a>";
                $(file_div).removeClass('attachment-uploading');
                sub_comment_ajax(delegation_id, 'AT', comment_text);
                var fileObj = $.fileuploader.getInstance($("#event_attachment_"+delegation_id)).findFile($(file_div).closest('li')[0]);
                fileObj.remove();
            } else {//event attachment fail
                $(file_div).text(text.slice(0, text.indexOf('Uploading...')) + "Upload Failed!Clear and Retry!");
                $(file_div).removeClass('attachment-uploading');
                $('#message_modal .modal-body').html('Could not upload attachment.Please check your internet connection!');
                $('#message_modal').modal('show');
            }
            if (!$('#upload_div_'+delegation_id).find('.attachment-uploading').length) {
                $('#upload_all_'+delegation_id).removeClass('disabled');
                $.fileuploader.getInstance($("#event_attachment_"+delegation_id)).enable();
            }
        }
    };
    xhr.send(postData);
    var text = $(file_div).text();
    $(file_div).append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Uploading.....");
}

function sub_comment_ajax(delegation_id, comment_type, comment_text) {
    $('#add_comment_'+delegation_id).attr('disabled', true);
    var select_target = $('#basicModal_'+delegation_id+' #select_target');
    var target_user = '';
    if (select_target.length) {
        target_user = select_target.val();
    } else {
        target_user = $('#delegation_user_'+delegation_id).val();
    }
    $.ajax({
        url: '/inhouse/post_comment/',
        type: 'POST',
        data: {
            'comment_text': comment_text,
            'del_id': delegation_id,
            'target': target_user,
            'comment_type': comment_type,
        },
        success: function (data) {
            var comment = data.comment;
            commentify([comment], delegation_id);
            $('#send_comment_'+delegation_id).val('');
        },
        complete: function () {
            window.setTimeout(function() {
                $('#add_comment_'+delegation_id).attr('disabled', false);
            }, 1000);
        }
    });
}

// to load right side bar events
// function render_template(template, comment) {
//     var replaceObj = [
//         ["[comment_from]", "author"]
//         ['[comment_time]', "created_date"],
//         ["[comment_text]", "text"],["[comment_to]", "target"]
//         ["[initials]", "initials"]
//     ];
//     for (var i =0;i<replaceObj;i++) {
//         template=template.replace(replaceObj[i][0], comment[replaceObj[i][1]]);
//     }
//     return template;
// }
function commentify(comments, delegation_id) {
    var activity_html = $("#activity-html").html();
    var chat_html = $("#chat-html").html();
    var username = $("#chat-html").data("username");
    var activity = '';
    var chat = '';
    var attachment = '';
    for ( var i = 0; i < comments.length; i++ ) {
        var auth = comments[i].author;
        var initials = comments[i].initials;
        var comment_type = comments[i].comment_type;
        var comment_text = comments[i].text;
        var created_date = comments[i].created_date;
        if (comment_type == 'EN' || comment_type == 'DN' ) {
            var new_comm = activity_html.replace('[comment_from]', auth).replace('[comment_time]', created_date).replace('[comment_text]', comment_text).replace('[initials]',initials);
            activity+=new_comm;
        } else {
            var new_comm = chat_html.replace('[comment_from]', auth).replace('[comment_time]', created_date).replace('[comment_text]', comment_text).replace('[initials]',initials);
            new_comm = new_comm.replace('[comment_to]', comments[i].target);
            if (comments[i].target == username) {
                new_comm = new_comm.replace("sent", "replies PArea").replace("<strong>", "<strong class='red-at'>");
            }
            if (comment_type=="CM") {
                chat+=new_comm;
            } else {
                attachment+=new_comm;
            }
        }
    }
    var activity_length = $("#activity-panel-"+delegation_id+" .panel-body").append(activity).children('.activity-div').length;
    $("#activity-heading-"+delegation_id+" .badge").text(activity_length);
    var chat_length = $("#chat-panel-"+delegation_id+" ul").append(chat).children().length;
    $("#chat-heading-"+delegation_id+" .badge").text(chat_length);
    var attachment_length = $("#attachment-panel-"+delegation_id+" ul.attachment-ul").append(attachment).children().length;
    $("#attachment-heading-"+delegation_id+" .badge").text(attachment_length);
}

function load_events(delegation_id, user_type) {
    $.ajax({
        url: "/inhouse/fetch_events",
        data: {
            "delegation_id": delegation_id,
            "user_type": user_type
        },
        type: "POST",
        success: function (data) {
            var json_data = data;
            var comments = json_data.comments;
	        var evnt_area = $('#activity-panel-'+delegation_id+' .panel-body');
            evnt_area.empty();
            commentify(comments, delegation_id);
            //evnt_area.scrollTop(evnt_area[0].scrollHeight);
            // $('#event-area_'+delegation_id).animate({ scrollTop: 99999  }, "fast");
        },
        error: function (xhr, status) {
            $('#message_modal .modal-body').html("Unable to load events");
            $('#message_modal').modal('show');
        },
    });
}

function hashCode (str) {
    var hash = 0, i, chr;
    if (str.length === 0) return hash;
    for (i = 0; i < str.length; i++) {
        chr   = str.charCodeAt(i);
        hash  = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    var hash = hash&0xFFFFFF;
    var r = Math.abs(hash>>16);
    var b = Math.abs(hash%256);
    var g = Math.abs(((hash-b-(r<<16))>>8));
    if (r<=b) {
        r = 0;
        if (b<128) {
            b+=128;
            b%=256
        }
    } else  {
        g = 0;
        if (r<128) {
            r+=128;
            r%=256;
        }
    }

    var color = [r,g,b];
    return color;
}
function colorify(delegation_id) {
    $('#comment-area_'+delegation_id).find('label.badge.badge-initials').each(function(index, element) {
        var auth = $(element).next().text();
        var color_hash = hashCode(auth);
        $(element).css('background-color', color_hash);
    });
}
function position_comarea(delegation_id, mark_id) {
    var mark_tag = $('#mark-'+delegation_id+"-"+mark_id)[0];
    var mark_y = $(mark_tag).offset().top;
    var margin_top = mark_y- $('#graphBoxTab').offset().top;
    var delement = $("#deliverable-"+delegation_id);
    var com_area = $("#comment-area_"+delegation_id);
    var ht = com_area.height();
    if (ht==0) {
        ht = $('#new-comment-area_'+delegation_id).height();
    }
    var neg_top = (mark_y-delement.offset().top)*(ht/delement.height());
    var margin_top = margin_top-neg_top;
    $(com_area).css('top', margin_top);
}
// function add_markers(delegation_id) {
//     var mark_dic = {};
//     var mark_id = 0;
//     $('#comment-container_'+delegation_id+' .comments').each(function(index, element) {
//         var cls = $(element).attr('class').match(/mark_\d+/)[0];
//         $(element).removeClass(cls);
//         var cst_id = $(element).attr('id').match(/\d+$/g);
//         var cst_arr = $("#deliverable-" + delegation_id + " cst" + cst_id);
//         if(cst_arr.length==2) {
//             var cst_top = get_nxt_top($("#deliverable-" + delegation_id + " cst" + cst_id)[0]);
//             if (!mark_dic[cst_top]) {
//                 mark_id++;
//                 var new_markcon = "<div class='markcon' id = 'markcon" + delegation_id + "_" + mark_id + "'><a href='#' id='mark-" + delegation_id + "-" + mark_id + "' class='mark-collapsed'><span class='glyphicon glyphicon-comment'></span></a></div>"
//                 $("#mark-area_" + delegation_id).append(new_markcon);
//                 $("#markcon" + delegation_id + "_" + mark_id).offset({top: cst_top});
//                 mark_dic[cst_top] = mark_id;
//             }
//             $(element).addClass('mark_'+mark_dic[cst_top]);
//         }
//         else {
//             $(element).remove();
//             $('#deliverable-'+delegation_id+' cst'+cst_id).remove();
//         }
//     });
// }
function add_markers(delegation_id) {
    var mark_dic = [];
    var mark_id = 0;
    $('#comment-area_'+delegation_id).css('left', $('#mark-area-'+delegation_id).offset().left-30);
    $('#comment-container_'+delegation_id+' .comments').each(function (index, element) {
        var cls = $(element).attr('class').match(/mark_\d+/)[0];
        $(element).removeClass(cls);
        var cst_id = $(element).attr('id').match(/\d+$/g);
        var cst_arr = $("#deliverable-" + delegation_id + " cst" + cst_id);
        if (cst_arr.length) {
            if (cst_arr.length==1) {
               $(cst_arr[0]).after('<cstno></cstno>'.replace(/no/g,cst_id));
            }
            hlt_comment(delegation_id, cst_id, 'cst'+cst_id);//must do this so that old orders work fine,can be removed later
            var cst_top = get_nxt_top($("#deliverable-" + delegation_id + " cst" + cst_id)[0]);
            var new_mark = -1;
            for (var i=0; i<mark_dic.length; i++) {
                if (Math.abs(mark_dic[i]-cst_top)<14) {
                    new_mark = i+1;
                }
            }
            if (new_mark==-1) {
                mark_id++;
                var new_markcon = "<div class='markcon' id = 'markcon" + delegation_id + "_" + mark_id + "'><a href='#' id='mark-" + delegation_id + "-" + mark_id + "' class='mark-collapsed'><span class='glyphicon glyphicon-comment'></span></a></div>"
                $("#mark-area-" + delegation_id).append(new_markcon);
                $("#markcon" + delegation_id + "_" + mark_id).offset({top: cst_top});
                new_mark = mark_id;
                mark_dic.push(cst_top);
            }
            $(element).addClass('mark_'+(new_mark));
        }
        // else {   Now comments, won't get deleted if there is any missing node
        //     $(element).remove();
        //     $('#deliverable-'+delegation_id+' cst'+cst_id).remove();
        // }
    });
}
function hlt_comment(delegation_id, block_id, className) {
    if (!className) {
        className = 'temp-hilight'
    }
    var cst = $('#deliverable-'+delegation_id+' cst'+block_id);
    var start = cst[0];
    var end = cst[1];
    //find lowest common ancestor of the two custom elements
    var start_ancestors = $(start).parentsUntil('.fr-wrapper');
    var end_ancestors = $(end).parentsUntil('.fr-wrapper');
    var i = start_ancestors.length-1;
    var j = end_ancestors.length-1;
    while(i>-1 && j>-1 && $(start_ancestors[i]).is(end_ancestors[j])) {
        i--;
        j--;
    }
    var common_ancestor = start_ancestors[i+1];
    //traverse the dom adding temp-hilight to each element until the second custom node is found
    while (!$(start).parent().is(common_ancestor)) {
        var start_par = start.parentNode;
        start = start.nextSibling;
        while(start) {
            if(start.nodeType==3) {
                $(start).wrap('<span class="text-node"></span>');
                start=start.parentNode;
            }
            if(!start.nodeName.startsWith('CST')) {
                $(start).addClass(className);
            }
            start=start.nextSibling;
        }
        start = start_par;
    }
    while (!$(end).parent().is(common_ancestor)) {
        var end_par = end.parentNode;
        end = end.previousSibling;
        while(end) {
            if(end.nodeType==3) {
                $(end).wrap('<span class="text-node"></span>');
                end=end.parentNode;
            }
            if(!end.nodeName.startsWith('CST')) {
                $(end).addClass(className);
            }
            end=end.previousSibling;
        }
        end = end_par;
    }
    var loop_ele = start.nextSibling;
    var i =0;
    while (loop_ele && !$(loop_ele).is(end)) {
        if (loop_ele.nodeType == 3) {
            $(loop_ele).wrap('<span class="text-node"></span>');
            loop_ele = loop_ele.parentNode;
        }
        if(!loop_ele.nodeName.startsWith('CST')) {
            $(loop_ele).addClass(className);
        }
        loop_ele = loop_ele.nextSibling;
    }
}
//new function to remove highlight
function rem_highlight(delegation_id) {
    $('#deliverable-'+delegation_id+' .temp-hilight').each(function(index,element){
        $(element).removeClass('temp-hilight');
        // if($(element).hasClass('text-node')){ No need to unwrap text-node span,as we are assigning a class to each text
        //     $(element).replaceWith($(element).html());
        // }
    });
}
//new function to remove a comment block
function rem_block(delegation_id,block_id) {
    $('#block-'+delegation_id+'-'+block_id).remove();
    $('#deliverable-'+delegation_id).find("cst"+block_id).remove();
    $('#deliverable-'+delegation_id).find(".cst"+block_id).removeClass('cst'+block_id);//remove the class as well
}


//new function to convert base64 encoded images to a blob
function dataURItoBlob(dataURI) {
    var byteString = atob(dataURI.split(',')[1]);
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    var mime = dataURI.split(',')[0].split(':')[1].split(';')[0];
    return window.URL.createObjectURL(new Blob([ab], { type: mime }));
}
//new function to insert custom tags
function create_new_block(delegation_id, count) {
    var new_block = "<cstno></cstno>".replace(/no/g, count);
    var blocks = $('#deliverable-' + delegation_id).froalaEditor('selection.blocks');
    var ranges = $('#deliverable-' + delegation_id).froalaEditor('selection.ranges', 0);
    var startContainer = ranges.startContainer;
    var endContainer = ranges.endContainer;
    var startOffset = ranges.startOffset;
    var endOffset = ranges.endOffset;
    var isEmpty = false;
    if ($(startContainer).is($(endContainer))) {
        if(startContainer.nodeType == 3) {
            var startHtml = $(startContainer)[0].wholeText;
            $(startContainer).replaceWith(startHtml.slice(0, startOffset) + new_block + startHtml.slice(startOffset, endOffset) + new_block + startHtml.slice(endOffset));
        } else {
            $(new_block).insertAfter($(startContainer.childNodes[endOffset-1]));
            $(new_block).insertBefore($(startContainer.childNodes[startOffset]));
        }
    } else {
        if (startContainer.nodeType == 3) {
            var startHtml = $(startContainer)[0].nodeValue.replace(/&quot;/g, "'");
            $(startContainer).replaceWith(startHtml.slice(0, startOffset) + new_block + startHtml.slice(startOffset));
        } else {
            //a weird occurence,when you change the alignment of image.The froala selection behave weirdly.
            if(startContainer.childNodes.length==0) {
                $(new_block).insertAfter(startContainer);
            } else if (startOffset == startContainer.childNodes.length) {
                $(startContainer).append(new_block);
            } else {
                var startElement = startContainer.childNodes[startOffset];
                $(new_block).insertBefore(startElement);
            }
        }
        if (endContainer.nodeType == 3) {
            var endHtml = $(endContainer)[0].nodeValue.replace(/&quot;/g, "'");
            $(endContainer).replaceWith(endHtml.slice(0, endOffset) + new_block + endHtml.slice(endOffset));
        } else {
            if (endContainer.childNodes.length==0) {
                $(new_block).insertBefore(endContainer);
            } else if (endOffset) {
                var endElement = endContainer.childNodes[endOffset - 1];
                $(new_block).insertAfter(endElement);
            } else {
                $(endContainer).prepend(new_block);
            }
        }
    }
}
// creating an editor
function create_editor(delegation_id,s3Hash,froalaKey) {
    $.FroalaEditor.DefineIcon('comment', {NAME: 'comment'});
    $.FroalaEditor.RegisterCommand('comment', {
        focus: false,
        undo: false,
        refreshAfterCallback: false,
        callback: function () {
            var delegation_id = $(".nav-tabs li.tab-press.active").find('a').attr('href').match(/\d+$/)[0];
            var ranges = $('#deliverable-' + delegation_id).froalaEditor('selection.ranges', 0);
            var container = $('#comment-container_' + delegation_id);
            var last = $(container).children().last('ul');
            $(container).show(0);
            var count = 1;
            var old_tags = null;
            //check if selection is empty
            var selection = window.getSelection();
            if (selection.rangeCount > 0) {
              var range = selection.getRangeAt(0);
              var clonedSelection = range.cloneContents();
              var div = document.createElement('div');
              div.appendChild(clonedSelection);
              //the selection must contain text.If it doesn't contain text then it must have an iframe or img or video element.
              if(!($(div).find('img, video, iframe').length || div.textContent.length)) {
                  return false;
              };
            }
            //find the count
            if(last.length) {
                count = parseInt($(last).attr('id').match(/\d+$/g))+1;
                //if last block doesn't contain any comment,means the user didn't press cancel
                //and instead has selected another to comment
                //so we need to cache the old tags and remove them later.Explanation ahead!
                if($(last).is(':empty')){
                    count-=1;
                    old_tags = $('#deliverable-'+delegation_id+ ' cst'+count);
                    $(last).remove();
                }
            }
            create_new_block(delegation_id, count);
            //old useless tags are removed here and not at the start because if any of the removed tags is inside the selection,
            //it will mess up the selection and content will be lost.
            if(old_tags) {
                $(old_tags).remove();
                //also remove temp-hilight before applying the class to the new selection!
                rem_highlight(delegation_id);
            }
            //check if any mark is active and deactivate it.If empty,remove it.
            var old_mark = $('#mark-area-' + delegation_id + ' .mark-active');
            if(old_mark.length) {
                var old_mark_id = $(old_mark).attr('id').match(/\d+$/g);
                $(old_mark).trigger('click');
                if (!$('#comment-container_' + delegation_id + ' .mark_' + old_mark_id).length) {
                    $(old_mark).parent().remove();
                }
            }
            //flag to check if a comment marker doesn't exist on this line
            var flag = true;
            var mark_ele = null;
            var cst_top = get_nxt_top($("#deliverable-" + delegation_id + " cst" + count)[0]);
            var markcons = $('#mark-area-'+delegation_id+' .markcon');
            for(var i= 0; i<markcons.length; i++){
                // if($(markcons[i]).offset().top==cst_top) {
                if(Math.abs($(markcons[i]).offset().top-cst_top)<14) {
                    flag = false;
                    mark_ele = markcons[i];
                    break;
                }
            }
            if(flag) {
                var new_markcon = "<div class='markcon' id = 'markcon-" + delegation_id + "-" + count + "'><a href='#' id='mark-" + delegation_id + "-" + count + "' class='mark-active'><span class='glyphicon glyphicon-comment'></span></a></div>"
                $("#mark-area-" + delegation_id).append(new_markcon);
                var cst_top = get_nxt_top($("#deliverable-" + delegation_id + " cst" + count)[0]);
                var mark_area_top = $("#mark-area-" + delegation_id).offset().top;
                $('#event-area_' + delegation_id).addClass('opace');
                $("#markcon-" + delegation_id + "-" + count).offset({top:cst_top});
            } else {
                $(mark_ele).find('a').trigger('click');
            }
            var mark_count = $('#mark-area-'+ delegation_id + ' .mark-active').attr('id').match(/\d+$/g);
            //added
            $('#comment-container_'+delegation_id +' .mark_'+mark_count+'.fa-minus-square-o').trigger('click');
            $("#comment-container_"+delegation_id).append("<ul id='block-"+delegation_id+'-'+count+"' class='fa fa-minus-square-o comments mark_"+ mark_count +"'></ul>");
            $("#comment-area_" + delegation_id + " .commentlist.comments").hide();
            $("#comment-area_" + delegation_id).show();
            //moved here
            hlt_comment(delegation_id, count, 'temp-hilight cst'+count);
            position_comarea(delegation_id, mark_count);
            $("#new_comment_" + delegation_id).focus();
        }
    });
    var config = {
        toolbarSticky: true,
        toolbarVisibleWithoutSelection: true,
        charCounterCount: false,
        fontSizeSelection: true,
        fontFamily: {
            'Malgun Gothic,sans-serif': 'Malgun Gothic',
            'Arial,Helvetica,sans-serif': 'Arial',
            'Georgia,serif': 'Georgia',
            'Impact,Charcoal,sans-serif': 'Impact',
            'Tahoma,Geneva,sans-serif': 'Tahoma',
            "'Times New Roman',Times,serif": 'Times New Roman',
            'Verdana,Geneva,sans-serif': 'Verdana'
        },
        fontFamilySelection: true,
        htmlAllowedTags: ['.*'],
        htmlAllowedEmptyTags: ['*'],
        htmlRemoveTags: [''],
        aviaryKey: 'eef016c43a344cc098c38d1a54966441',         // aviary plugin for image editing
        imageUploadToS3: s3Hash,                               // once attached direct upload to S3
        imageUploadRemoteUrls: false,
        imageMaxSize: 1024*1024*20,                            // image max-upload size 20MB
        videoUploadToS3: s3Hash,
        fileUploadToS3: s3Hash,
        videoAllowedTypes: ['mp3','mp4', 'mkv', 'mov', 'wmv', 'avi', 'webm', 'gif', 'gifv', 'ogg'],
        htmlUntouched: true,
        pastePlain: false,
        wordPasteModal: false,
        toolbarButtons: ['comment', 'bold', 'italic', 'underline', 'strikeThrough', 'color', 'emoticons', 'paragraphFormat', 'align','fontFamily','fontSize', '-', 'quote','formatOL', 'formatUL', 'indent', 'outdent', 'insertImage', 'insertLink', 'insertVideo', 'insertTable', 'undo', 'redo', 'subscript', 'superscript'],
        imageEditButtons: ['imageAlign', 'imageRemove', '|', 'imageLink', 'linkOpen', 'linkEdit', 'linkRemove', 'aviary', '-', 'imageDisplay', 'imageStyle', 'imageAlt', 'imageSize',],
        imageInsertButtons: ['imageBack', '|', 'imageUpload', 'imageByURL',],
        shortcutsEnabled: ['show', 'bold', 'italic', 'underline', 'strikeThrough', 'indent', 'outdent', 'undo', 'redo', 'insertImage', 'createLink','newParagraph',],
    };
    if (froalaKey) {
        config.key = froalaKey;
    }
    var textElement = null;
    if($("#deliverable-" + delegation_id).length) {
        $('#deliverable-' + delegation_id).froalaEditor(config);
        $('#menu-div-'+delegation_id).show();
        $('#loader-gif-'+delegation_id).hide();
        $('#mark-area-' + delegation_id).empty();
        var active_tab = $(".nav-tabs li.tab-press.active").find('a').attr('href').match(/\d+$/);
        $('#mark-area-' + delegation_id).show();
        if (active_tab && (delegation_id == active_tab[0])) {
            add_markers(delegation_id);
        }
        textElement = $('#deliverable-' + delegation_id+' .fr-element');
    } else {
        config.toolbarButtons.splice(11,1);
        config.toolbarInline=false;
        config.toolbarButtons.shift();
        config.toolbarContainer='#toolbar-container-'+delegation_id;
        config.heightMin = 400;
        config.zIndex= 2500;
        var textArea = $('#text_'+delegation_id);
        if (textArea.length) {
            $('#text_' + delegation_id).froalaEditor(config);
            textElement = $('#del_add_form_' + delegation_id + ' .fr-element');
        }
    }
    // if(textElement) {
    //     Countable.on(textElement[0], function (counter) {
    //         $('#count-div-' + delegation_id + ' .word_count').text(counter.words);
    //     }, {
    //         ignoreZeroWidth: true
    //     });
    // }
}
$('.deliverable-text').on('froalaEditor.initialized', function(e, editor) {
    var imageUploading = 0;
    var failedUploads = 0;
    var delegation_id = e.target.id.match(/\d+$/)[0];
    editor.events.on('image.beforeUpload', function () { //while uploading hide the menu
        if (!imageUploading) {
            $('.menu-div').hide();
            $('.tab-press').css('pointer-events', 'none');
        }
        imageUploading++;
    });
    editor.events.on('image.error', function (error) {     //image upload error having file of size more than 20MB
        var text = '';
        if (error.code == 5) {
            text = "Maximum size limit exceeded!Please make sure the size of each image is less than 20MB.";
        } else if (error.code == 6) {
            text = "Wrong file format!Only select one of these formats: gif,jpeg, jpg, png";
        } else {
            failedUploads++;
        }
        if (text) {
            $('#message_modal .modal-body').html(text);
            $('#message_modal').modal('show');
        }
    });
    editor.events.on('image.inserted image.error image.replaced', function () {  // error on image upload,s3 response error,
        imageUploading--;
        if (!imageUploading) {
            $('#menu-div-'+delegation_id).show();
            if (failedUploads) {
                $('img[data-fr-image-pasted="true"], img.fr-uploading').remove();
                $('#message_modal .modal-body').html("Some images couldn't be uploaded.Please check your internet connection and retry adding those.");
                $('#message_modal').modal('show');
            }
            failedUploads = 0;
            $('.tab-press').css('pointer-events', '');
        }
    });
    editor.events.on('toolbar.show', function () {
        var win_text = window.getSelection().toString().replace(/['";:,.?\u200B\u00bf\-\u2013\!\u00a1]+/g,"").match(/\S+/g);
        if(win_text) {
            $('#count-div-'+delegation_id+' .selected_count').text(win_text.length);
            $('#count-div-'+delegation_id+' .sel_span').show();
        }
    });
    editor.events.on('toolbar.hide', function () {
        $('#count-div-'+delegation_id+' .sel_span').hide();
    });
    editor.events.on('paste.after', function () {
        var delegation_id = $(".nav-tabs li.tab-press.active").find('a').attr('href').match(/\d+$/)[0];
        $('#deliverable-'+delegation_id+' .fr-element').find('div').each(function(index,element) {
            $(element).replaceWith($(element).html());
        });
        // Countable.count($('#deliverable-'+delegation_id+' .fr-element')[0], function (counter){$('#count-div-'+delegation_id+' .word_count').text(counter.words);});
    });
    editor.events.on('video.inserted', function (vdo) {
        $(vdo).find('video').attr('preload', 'metadata');
    });
    editor.events.on('video.beforeRemove', function (vdo) {
        //change to disable video delete
        if(!confirm('Are you sure?')) {
            return false;
        }
    });
    editor.events.on('paste.beforeCleanup', listCleanup);
    editor.events.on('paste.afterCleanup', function( clipboardHtml ) {//copy paste problem
        var pasteHtml = $('<div/>');
        $(pasteHtml).append(clipboardHtml);
        $(pasteHtml).find('*').each(function (index, element) {
            if(element.tagName.startsWith('CST')) {
                $(element).remove();
            }
            else {
                var eleClass = element.className;
                $(element).attr('class',eleClass.replace(/(cst\d+|temp-hilight)/g,'').trim());
            }
        });
        return $(pasteHtml).html();
    });
});
$('.init_text').on('froalaEditor.initialized', function(e, editor) {
    var delegation_id = $(this).attr('id').match(/\d+$/)[0];
    var imageUploading = 0;
    var failedUploads = 0;
    editor.events.on('image.beforeUpload video.beforeUpload', function () {
        $('#save_send_'+delegation_id).parent().find('.btn').addClass('disabled');
        imageUploading++;
    });
    editor.events.on('image.error', function (error) {   // image size > 20 MB
        if ( error.code == 5) {
            $('#message_modal .modal-body').html("Maximum size limit exceeded!Please make sure the size of each image is less than 20MB.");
            $('#message_modal').modal('show');
        }
        else {
            failedUploads++;
        }
    });
    editor.events.on('image.inserted video.inserted image.error image.replaced', function () {  //image insert replace and upload error
        imageUploading--;
        if (!imageUploading) {
            $('#save_send_'+delegation_id).parent().find('.btn').removeClass('disabled');
            if (failedUploads) {
                $('img[data-fr-image-pasted="true"], img.fr-uploading').remove();
                $('#message_modal .modal-body').html("Some images couldn't be uploaded.Please check your internet connection and retry adding those.");
                $('#message_modal').modal('show');
            }
            failedUploads = 0;
        }
    });
    editor.events.on('paste.beforeCleanup', listCleanup);
    // editor.events.on('paste.after', function () {
    //     Countable.count($('#del_add_form_' + delegation_id + ' .fr-element')[0], function (counter){$('#count-div-'+delegation_id+' .word_count').text(counter.words);});
    // });
});
$('.version-dropdown select').on('change', function () {
    var delegation_id = $(this).closest('.version-dropdown').attr('id').match(/\d+/g)[0];
    var version_str = $(this).val();
    var view = 'PV';
    if (version_str.indexOf('CLIENT')>-1) {
       view = 'CV';
    } else if (version_str.indexOf('SPIRAL')>-1) {
       view = 'SV';
    }
    var version = version_str.match(/\d+/g)[0];
    $(this).prop('disabled', true);
    $(this).selectpicker('refresh');
    get_deliverable(delegation_id, view, version);
});
function get_deliverable(delegation_id, view_type, version) {
    //changes
    $('#save-list-'+delegation_id).prop('disabled', true);
    $('#save-list-'+delegation_id).selectpicker('refresh');
    $.ajax({
        url: '/inhouse/get_prev_versions',
        type: 'POST',
        data: {
            'delegation_id': delegation_id,
            'version': version,
            'showing_type': view_type
        },
        success: function(data) {
            $('#created-'+delegation_id).text(data.creation_date)
            $('#creator-'+delegation_id).text(data.version_author)
            $('#comment-container_'+delegation_id).empty();
            $('#mark-area-'+delegation_id).empty();
            $('#mark-area-'+delegation_id).show();
            $('#deliverable-'+delegation_id).froalaEditor('html.set',data.deliverable_text);
            $('#comment-container_'+delegation_id).append(data.deliverable_comments);
            $('#comment-area_'+delegation_id+' .comments').hide();
            // Countable.count($('#deliverable-'+delegation_id+' .fr-element')[0], function (counter){$('#count-div-'+delegation_id+' .word_count').text(counter.words);});
            if(data.is_latest_version) {
                $('#deliverable-'+delegation_id).froalaEditor('edit.on');
                $('#new-comment-area_'+delegation_id).show();
                $('#save-list-'+delegation_id+" select").prop('disabled', false);
                $('#save-list-'+delegation_id+" select").selectpicker('refresh');
            } else {
                $('#deliverable-'+delegation_id).froalaEditor('edit.off');
                $('#new-comment-area_'+delegation_id).hide();
                $('#comment-container_'+delegation_id+' .action-link').hide();
            }
            var cntr =0;
            var load_arr = $('#deliverable-'+delegation_id+' img');
            if(load_arr.length) {
                $(load_arr).on('load', function () {
                    cntr++;
                    if (cntr == load_arr.length) {
                        add_markers(delegation_id);
                    }
                });
            }
            else {
                add_markers(delegation_id);
            }
            $('#comment-container_'+delegation_id + ' .fa-minus-square-o').trigger('click');

        },
        complete: function (data) {
            $('#comment-area_'+delegation_id).hide();
            $('#version-dropdown-'+delegation_id+" select").prop('disabled', false);
            $('#version-dropdown-'+delegation_id+" select").selectpicker('refresh');
        }
    });
}

function get_str(el) {
  var a = [el.nodeName.toLowerCase()];
  var atts = el.attributes;
  for (var i=0; i < atts.length; i++) {
    a.push(atts[i].name + '="' + atts[i].value + '"');
  }
  return a.join(" ");
}

// to get all the versions for a role
function get_versions(view_type, delegation_id, select_type) {
    var view_type = $(view_type).val();
    if(select_type == 'version') {
        var select = $('#showing_'+delegation_id);
    } else {
        var select = $('#comparison_'+delegation_id);
    }
    $.ajax({
        url: '/inhouse/get_versions',
        type: 'POST',
        data: {
            'delegation_id': delegation_id,
            'view_type': view_type,
        },
        success: function(data) {
            select.empty();
            var options = data.version_options;
            if(options) {
                options = JSON.parse(options);
                $.each(options, function(key, val) {
                    if(key == '') {
                        $(select).prepend( $("<option selected disabled>")
                            .val(key)
                            .html(val)
                        );
                    } else {
                        $(select).append( $("<option>")
                            .val(key)
                            .html(val)
                        );
                    }
                });
            }
        },
        beforeSend: function () {
            $(select).prop('disabled','disabled');
        },
        complete: function (data) {
            $(select).prop('disabled',false);
        }
    });
}

//changes 28-6-17
function post_comment(delegation_id, first_name, last_name){
    var new_comment = $("#new_comment_"+delegation_id).val().trim();
    var mark_count = $("#mark-area-"+ delegation_id +" .mark-active").parent().attr('id').match(/\d+$/g);
    if (new_comment) {
        var active_com = $('#comment-container_' + delegation_id + ' .fa-minus-square-o');
        if (active_com.length) {
            var count = $(active_com).attr('id').match(/\d+$/g)[0];
            $("#new_comment_" + delegation_id).val('');
            var editComment = "<a class='action-link edit-comment'" + " href='#'>Edit&nbsp&nbsp</a>";
            var delComment = "<a class='action-link del-comment'" + " href='#'>Delete&nbsp&nbsp&nbsp</a>";
            var postComment = "<a class='action-link post-comment'" + " href='#' style='display:None'>Post&nbsp&nbsp</a>";
            var closeComment = "<a class='action-link close-comment'" + " href='#' style='display:None'>Close</a>";
            var first_letter = first_name.charAt(0).toUpperCase();
            var last_letter = last_name.charAt(last_name.length - 1).toUpperCase();
            var initials = first_letter + last_letter;
            var cmnt_li = '<li><label class="badge badge-initials">' + initials + '</label><div class="author-name right-of-avatar">' + first_name+' '+last_name + '</div><div class="sdcomment right-of-avatar comment">' + new_comment + "</div><p>" + editComment + delComment + postComment + closeComment + "</p></li>";
            var color_hash = hashCode(first_name+' '+last_name);
            $(cmnt_li).appendTo(active_com).find('.badge').css('background-color','rgb(' + color_hash + ')');
            position_comarea(delegation_id, mark_count);
            //changed
            $(active_com).scrollTop($(active_com).prop('scrollHeight'));
        }
        else{
            $('#message_modal .modal-body').html('No block active!Click on some to block to comment!');
            $('#message_modal').modal('show');
        }
    } else {
        $('#message_modal .modal-body').html("Can't post empty comment!");
        $('#message_modal').modal('show');
    }
}

function cancel_comment(delegation_id) {
    var cur_mark = $('#mark-area-'+delegation_id+' .mark-active');
    var nums = $(cur_mark).attr('id').match(/\d+$/g);
    var mark_id = nums[0];
    //toggle cur_mark
    $(cur_mark).trigger('click');
    //check if the last comment block is empty and if it is delete
    var last_com = $('#comment-container_'+delegation_id).children().last()
    if(!$(last_com).children().length) {
        //remove the tags
        var block_id = $(last_com).attr('id').match(/\d+$/g);
        rem_block(delegation_id, block_id);
        //changed
        if (!$('#comment-container_' + delegation_id + ' .mark_' + mark_id).length) {
            $('#markcon-' + delegation_id + '-' + mark_id).remove();
        }
    }
    //remove hilight
    rem_highlight(delegation_id);
    $('#event-area_'+delegation_id).removeClass('opace');
}

$(document).on('click', ".edit-comment", function() {
    var delegation_id = $(".nav-tabs li.tab-press.active").find('a').attr('href').match(/\d+$/)[0];
    var txtbx = $(this).closest("li");
    $(txtbx).parent().find('.sdcomment[contenteditable="true"]').next().find('.close-comment').trigger('click');
    $(this).hide();
    txtbx.find(".sdcomment").attr('contenteditable','true').addClass('form-control').focus();
    txtbx.find(".del-comment").hide(0);
    txtbx.find(".post-comment").show(0);
    txtbx.find(".close-comment").show(0);
    $(this).parent().append('<a class="temp-comment-a" style="display:none;">'+ txtbx.find(".sdcomment").text() + '</a>');
    $("#new-comment-area_"+delegation_id).hide(0);
    return false;
});

$(document).on('click', ".post-comment", function () {
    var delegation_id = $(".nav-tabs li.tab-press.active").find('a').attr('href').match(/\d+$/)[0];
    var txtbx = $(this).closest("li");
    if ($(txtbx).find('.sdcomment').text()) {   //post a comment only if textbox is not empty
        //changes
        var user_name = $('#user-name').text().trim();
        $(txtbx).find('.author-name').text(user_name);
        var badge = $(txtbx).find('.badge');
        $(badge).text((user_name.charAt(1) + user_name.slice(-1)).toUpperCase());
        $(badge).css('background-color', 'rgb(' + hashCode(user_name) + ')')
        //end here
        $(this).hide();
        txtbx.find(".sdcomment").attr('contenteditable', 'false').removeClass('form-control');
        txtbx.find(".edit-comment").show();
        txtbx.find(".del-comment").show();
        txtbx.find(".close-comment").hide();
        $(this).parent().find('.temp-comment-a').remove();
        $("#new-comment-area_" + delegation_id).show(0).find("textarea").focus();
    } else {
        $('#message_modal .modal-body').html("Can't post empty comment!");
        $('#message_modal').modal('show');
    }
    return false;
});

$(document).on('click', ".del-comment", function() {
    var delegation_id = $(".nav-tabs li.tab-press.active").find('a').attr('href').match(/\d+$/)[0];
    var li = $(this).closest("li");
    var block_id = li.parent().prop('id');
    var index = $(li).index();
    var params= block_id+','+index;
    $('#check_modal .modal-body').text('Are you sure you want to delete this comment?');
    $('#modal_confirm_btn').data({"operation": 'delete_check', "params": params });
    $('#check_modal').modal('show');
    return false;
});

function del_comment(params) {
    params=params.split(',');
    var block_id = params[0];
    var index = params[1];
    var block  = $('#'+block_id);
    var li = $(block).children().eq(index);
    var delegation_id  = block_id.split('-')[1];
    var b_id = block_id.split('-')[2];
    var mark_id= $('#mark-area-'+delegation_id+' .mark-active').attr('id').match(/\d+$/g);
    if(li.parent().children().length == 1) {
        rem_block(delegation_id, b_id);
        if(!$('#comment-container_'+delegation_id+' .mark_'+mark_id).length) {
            $('#markcon'+delegation_id+'_'+mark_id).remove();
            $('#comment-area_'+delegation_id).hide();
            $('#event-area_'+delegation_id).removeClass('opace');
        }
        rem_highlight(delegation_id);
    } else {
        li.remove();
        position_comarea(delegation_id, mark_id);
    }
}
function approve_deliverable(delegation_id) {
    $('#approve_type_'+delegation_id).val("approve");
    $('#approve_del_id_'+delegation_id).val(delegation_id);
    $('#form_approve_'+delegation_id).submit();
}

$(document).on('click', '.fa-minus-square-o',function(e) {
    if (e.target == this) {
        var nums = $(this).attr('id').match(/\d+/g);
        var delegation_id = nums[0];
        var mark_id = nums[1];
        $(this).children().hide();
        $(this).toggleClass('fa-minus-square-o fa-plus-square-o');
        rem_highlight(delegation_id);
    }
    return false;
});
$(document).on('click', '.fa-plus-square-o',function(e){
    if (e.target == this) {
        var nums = $(this).attr('id').match(/\d+/g);
        var delegation_id = nums[0];
        var block_id = nums[1];
        var cst = $('#deliverable-'+delegation_id+' cst'+block_id);
        var hlt = true;
        if (cst.length!=2) {
            $(cst).remove();
            hlt = reinsertNodes(delegation_id, block_id);
        }
        $('#comment-container_' + delegation_id + ' .fa-minus-square-o').trigger('click');
        $(this).children().show();
        $(this).toggleClass('fa-minus-square-o fa-plus-square-o');
        if (hlt) {
            hlt_comment(delegation_id, block_id, 'temp-hilight cst'+block_id);
        }
        if ($('#deliverable-'+delegation_id+' .cst' + block_id).length) {
            $(this).removeClass('removed-text');
        } else {
            $(this).addClass('removed-text');
        }
        var mark_id = $('#mark-area-' + delegation_id + ' .mark-active').attr('id').match(/\d+$/g);
        position_comarea(delegation_id, mark_id);
    }
    return false
});

$(document).on('click', ".close-comment", function() {
    var delegation_id = $(".nav-tabs li.tab-press.active").find('a').attr('href').match(/\d+$/)[0];
    var txtbx = $(this).closest("li");
    txtbx.find(".sdcomment").text($(this).parent().find('.temp-comment-a').text());
    $(this).hide(0);
    txtbx.find(".sdcomment").attr('contenteditable','false').removeClass('form-control');
    txtbx.find(".edit-comment").show(0);
    txtbx.find(".del-comment").show(0);
    txtbx.find(".post-comment").hide(0);
    $(this).parent().find('.temp-comment-a').remove();
    $("#new-comment-area_"+delegation_id).show(0).find("textarea").focus();
    return false;
});
$(document).on('click','.mark-active', function() {
    var nums = $(this).attr('id').match(/\d+/g);
    var delegation_id = nums[0];
    var mark_id = nums[1];
    $('#comment-container_'+delegation_id+' .mark_'+mark_id+' .sdcomment[contenteditable="true"]').next().find('.close-comment').trigger('click');
    $('#comment-container_'+delegation_id+' .mark_'+mark_id).hide();
    $('#comment-container_'+delegation_id+' .fa-minus-square-o').trigger('click');
    //remove hilight
    rem_highlight(delegation_id);
    $(this).toggleClass('mark-active mark-collapsed');
    $('#event-area_'+delegation_id).removeClass('opace');
    $('#comment-area_'+delegation_id).hide(0);
    return false;
});
$(document).on('click','.mark-collapsed', function() {
    var nums = $(this).attr('id').match(/\d+/g);
    var delegation_id = nums[0];
    var mark_id = nums[1];
    $('#mark-area-'+delegation_id+' .mark-active').trigger('click');
    //hide already active element if any
    $('#comment-container_'+delegation_id+' .mark_'+mark_id).show();
    $(this).toggleClass('mark-active mark-collapsed');
    position_comarea(delegation_id,mark_id);
    $('#comment-container_'+delegation_id+' .mark_'+mark_id).first().trigger('click');
    $('#comment-area_'+delegation_id).show(0);
    $('#event-area_'+delegation_id).addClass('opace');
    return false;
});
$('.delete_all_comments').on('click', function() {
    var delegation_id = $(this).attr('id').split('_')[2];
    $('#check_modal .modal-body').text('Are you sure you want to delete all comments?');
    $('#modal_confirm_btn').data({"operation": 'delete_all_check', "params": delegation_id });
    $('#check_modal').modal('show');
});

function delete_all (delegation_id) {
    $('#comment-container_'+delegation_id).children().each(function(index, element){
        var block_id = $(element).attr('id').match(/\d+$/)[0];
        rem_block(delegation_id, block_id);
        $('#mark-area-'+delegation_id).empty();
    });
    rem_highlight(delegation_id);
    $('#comment-area_'+delegation_id).hide();
    $('#event-area_'+delegation_id).removeClass('opace');
}
function get_nxt_top(element) {
    var nxt = element.nextSibling;
    var top = -1;
    if (!nxt) {
        top = $(element).offset().top;
    } else if((nxt.nodeType == 3)||(nxt.tagName.startsWith('CST'))) {
        $(nxt).wrap('<span></span>');
        if(nxt.nodeType!=3) {
            $(nxt).after('random text');
        }
        top = $(nxt).parent().offset().top;
        $(nxt).parent().replaceWith(nxt);
    } else {
        top = $(nxt).offset().top;
    }
    return top;
}
//changed
function save_deliverable (delegation_id, save_type) {

    $('#message_modal .modal-body').html('Please wait!');
    $('#message_modal .btn').prop('disabled',true);
    $('#message_modal').modal('show');
    $('.sdcomment[contenteditable="true"]').next().find('.close-comment').trigger('click');
    if($('#mark-area-' + delegation_id + ' .mark-active').length) {
        cancel_comment(delegation_id);
    }
    fixDeletedNodes(delegation_id);//if there are one or more deleted nodes,reinsert them.
    $("[data-id]").removeAttr('data-id');
    $("[data-bid]").removeAttr('data-bid');
    var del_text = $('#deliverable-'+delegation_id+' .fr-element').html();
    var del_comments = $('#comment-container_'+delegation_id).html();
    $.ajax({
        url: window.location.pathname + window.location.search,
        data: {
            'delegation_id': delegation_id,
            'deliverable': del_text,
            'comments': del_comments,
            'save_type': save_type
        },
        method: 'POST',
        success: function (response) {
            $('#message_modal').data('reload', true);
            $('#message_modal .modal-body').html(response.message);
            // uploadFile(file_data, response.data, response.url, response.upload_url);
        },
        error: function (response) {//added error function for event attachment
            $('#message_modal .modal-body').html(response.responseJSON.message);
            // $('#message_modal').modal('show');
        },
        complete: function (response) {
            $('#message_modal .btn').prop('disabled',false);
        }
    });
}
$('.modal_btn_div .save_draft,.save_send').on('click', function() {
    $(this).parent().find('.btn').addClass('disabled');
});

$('#message_modal').on('hidden.bs.modal', function () {
    if ($(this).data('reload')===true) {
        window.location.reload();
    }
});

// ms office code..need to be changed to accommodate more cases in future

function get_msodata(element) {
    var msoData;
    var eleStyle= $(element).attr('style');
    if(eleStyle) {
        var mso = eleStyle.match(/mso-list:.*?(;|$)/g);
        // console.log('ok',mso,eleStyle);
        if (mso) {
            msoData = mso[0].match(/\d+/g);
        }
    }
    if (!msoData) {
        msoData = [];
    }
    return msoData;
}
function deromanize (str) {
	var	str = str.toUpperCase(),
		validator = /^M*(?:D?C{0,3}|C[MD])(?:L?X{0,3}|X[CL])(?:V?I{0,3}|I[XV])$/,
		token = /[MDLV]|C[MD]?|X[CL]?|I[XV]?/g,
		key = {M:1000,CM:900,D:500,CD:400,C:100,XC:90,L:50,XL:40,X:10,IX:9,V:5,IV:4,I:1},
		num = 0, m;
	if (!(str && validator.test(str)))
		return false;
	while (m = token.exec(str))
		num += key[m[0]];
	return num;
}
function numToStr(num) {
    var str = '';
    while (num) {
        str+=String.fromCharCode(num%26+1);
        num = parseInt(num/26);
    }
    return str
}
function strToNum(str) {
    str = str.toLowerCase();
    var num = 0;
    for (var i = 0; i < str.length; i++) {
        num *= 26;
        num += (str.charCodeAt(i) - 96);
    }
    return num;
}
function checkIfRoman(ol,element) {
    if ($(ol).children().length==1) {
        var olType = $(ol).attr('type').toLowerCase();
        //enter only if the type of Ol is alphabetic
        if (olType == 'a') {
            var olStart = $(ol).attr('start');
            var curIndexStr = $(element).children().first().text().trim().slice(0, -1);
            var curIndex = strToNum(curIndexStr);
            //check if the prev li index - cur li index is 1,if it's then it's not roman
            if(curIndex-olStart!=1) {
                var romStart = deromanize(curIndexStr);
                if(romStart) {
                    var type = olType==$(ol).attr('type')?'i':'I';
                    $(ol).attr({'type': type,'start': romStart-1 });
                }
            }
        }
    }
}
function olStart(element) {
    var olStr = $(element).children().first().text().trim().slice(0, -1);
    var num = olStr.match(/\d+/g);
    var type = '1';
    var start = 1;
    if(num) {
        type = '1';
        start = parseInt(num);
    }
    else {
        type = 'A';
        var lowerCase = olStr.toLowerCase();
        if (lowerCase == olStr) {
            type = 'a';
        }
        start = strToNum(olStr);
    }
    return {'type':type,'start': start};
}
function listCleanup(clipboardhtml) {
    try {
        var body = clipboardhtml.match(/<body[\s\S]*?\/body>/g);
        var bodyOpen ='',bodyClose='';
        if (body) {
            clipboardhtml= body[0];
            bodyOpen = "<body>";
            bodyClose = "</body>";
        }
        var pasteHtml = $('<div/>');
        pasteHtml = pasteHtml.append(clipboardhtml);
        //mso-list:l4 level1 lfo4
        var prevLevel = 1;
        var prevL = -1;
        var prevLfo = -1;
        var prevOl = null;
        $(pasteHtml).find('*').not("ol,ul,li").each(function (index, element) {
            var curMso = get_msodata(element);
            var olStr = $(element).children().first().text().trim();
            if (curMso.length && olStr.length > 1) {
                var prevMso = get_msodata($(element).prev()[0]);
                var ol = prevOl;
                if (prevMso.length && curMso[2] == prevLfo) {
                    if (curMso[1] > prevLevel) {
                        ol = $('<ol/>');
                        $(ol).attr(olStart(element));
                        $(prevOl).append(ol);
                    }
                    else if (curMso[1] < prevLevel) {
                        ol = $(prevOl).parent();
                    }
                    else {
                        checkIfRoman(ol, element);
                    }
                }
                else {
                    ol = $('<ol/>');
                    $(ol).attr(olStart(element));
                    $(ol).insertBefore(element);
                }
                var li = $('<li/>');
                $(element).children().first().remove();
                $(li).html($(element).html());
                $(ol).append(li);
                //updates
                prevL = curMso[0];
                prevLevel = curMso[1];
                prevLfo = curMso[2];
                prevOl = ol;
                $(element).addClass('list-remove');
            }
        });
        $(pasteHtml).find('.list-remove').remove();
        return bodyOpen+$(pasteHtml).html()+bodyClose;
    }
    catch(e) {
        console.log("Error", e.stack);
        console.log("Error", e.name);
        console.log("Error", e.message);
    }
}

function CopyToClipboard(containerid) {
    if (document.selection) {
        var range = document.body.createTextRange();
        range.moveToElementText(document.getElementById(containerid));
        range.select().createTextRange();
    } else if (window.getSelection) {
        var range = document.createRange();
         range.selectNode(document.getElementById(containerid));
         window.getSelection().addRange(range);
    }
    document.execCommand("Copy");
    $('#message_modal .modal-body').html('Text Copied!');
    $('#message_modal').modal('show');
}
function exportAsDoc(delegation_id, title) {
    var content_div = $('#deliverable-'+delegation_id+' .fr-element').clone();
    content_div.find('video,iframe').each(function(index, element){
       $(element).replaceWith('<p>'+$(element).attr('src')+'</p>').html();
    });
    var body = '<body>'+content_div.html()+'</body>';
    var head = '<head><style>body{font-family: Georgia }a{text-decoration:underline}table td,th {border: 1.5px solid black;} table{border-collapse: collapse}</style><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></head>';
    var content='<!DOCTYPE html><html>'+head+body+'</html>';
    var title = $('#content-title').text().trim();
    var converted = htmlDocx.asBlob(content);
    saveAs(converted, title+'.docx');
    // var o = {
    //     filename: title+'.doc',
    //     html: content,
    // };
    // $(document).googoose(o);
}
function exportPlainText(delegation_id) {
    var element = $('#deliverable-'+delegation_id);
    var title = $('#content-title').text();
    $(element).find('img,iframe,video').each(function(index, element) {
        $('<span class="temp-plain-paste-node"></span>').insertBefore(element).text($(element).attr('src'));
    });
    var str = element[0].innerText.replace(/\r?\n/g, "\r\n");
    $('.temp-plain-paste-node').remove();
    var blob = new Blob([str], {type: "text/plain;charset=utf-8"});
    var fsaver = saveAs(blob, title+".txt");
}

function reinsertNodes (delegation_id, cst_id) {//reinsertion is done here if there is an element with a cstclass
    var newBlock = '<cstno></cstno>'.replace(/no/g, cst_id);
    var first = $('#deliverable-'+delegation_id+' .cst' + cst_id).first();
    var last  = $('#deliverable-'+delegation_id+' .cst' + cst_id).last();
    if (first.length) {
        $(first).before(newBlock);
        $(last).after(newBlock);
        return true;
    }
    return false;
}

function fixDeletedNodes (delegation_id) {//function to handle deleted node issue
    $('#comment-container_'+delegation_id+' .comments').each(function (index, element) {
       var cst_id = $(element).attr('id').match(/\d+$/g)[0];
       var p_id = parseInt($(element).attr('data-bid'));
       var cst = $('#deliverable-'+delegation_id+' cst'+cst_id);
       if (cst.length!=2) {//if there is no element with a cst class, then it is inserted at the top of the nearest paragraph.
           $(cst).remove();
           if (!$('#deliverable-' + delegation_id + ' .cst' + cst_id).length) {
               if (isNaN(p_id)) {
                   $(element).remove();
               }
               else {
                   var cur_p_id = -1;
                   var ele = $('#deliverable-' + delegation_id + ' .fr-element');
                   $('#deliverable-' + delegation_id + ' .fr-element').children().each(function (index, element) {
                       ele = element;
                       cur_p_id = $(element).attr('data-id');
                       if (cur_p_id && parseInt(cur_p_id) >= p_id) {
                           return false;
                       }
                   });
                   $(element).addClass('removed-text');
                   $(ele).prepend("<cstno></cstno><cstno></cstno>".replace(/no/g, cst_id));
               }
           }
           else {
               reinsertNodes(delegation_id, cst_id);
           }
       }
    });
}
function addDataId(delegation_id) {//comment to para mapping
    var cstSet = new Set();
    var mapCommentToPara = function (nodeName, pid) {
        var cst_id = nodeName.match(/\d+$/g)[0];
        if (!cstSet.has(cst_id)) {
            cstSet.add(cst_id);
            $('#block-' + delegation_id + '-' + cst_id).attr('data-bid', pid);
        }
    }
    $('#deliverable-'+delegation_id).children().each(function (index, element) {
        if(!$(element).prop('tagName').startsWith('CST')) {
            $(element).attr('data-id', index);
            $(element).find('*').each(function (indx, element) {
                var nodeName = $(element).prop('tagName');
                if (nodeName.startsWith('CST')) {
                    mapCommentToPara(nodeName, index);
                }
            });
        }
    });
}

var onlineStatus = 1;
window.setInterval(function () {
    if (!window.navigator.onLine && onlineStatus) {
        onlineStatus = 0;
        $('#message_modal .modal-body').html('Internet Disconnected!Please reconnect');
        $('#message_modal').modal('show');
    } else if (!onlineStatus && window.navigator.onLine) {
        onlineStatus = 1;
    }
}, 3000);


$('.save-list select').on('change', function () {
    var delegation_id = $(this).closest('.save-list')[0].id.match(/\d+/g)[0];
    var message = "Are you sure you want to save and send to " + $(this).val() + "?";
    showConfirmModal(message, 'save_check', [delegation_id, $(this).val()]);
    $(this).val("").selectpicker("refresh");
});
$('.export-list select').on('change', function () {
    var delegation_id = $(this).closest('.export-list')[0].id.match(/\d+/g)[0];
    alert($(this).val());
    if ($(this).val()=="Word Doc") {
        exportAsDoc(delegation_id);
    } else {
        exportPlainText(delegation_id);
    }
    $(this).val("").selectpicker("refresh");
});

function showConfirmModal(message, operation, params) {
    $('#check_modal .modal-body').text(message);
    $('#modal_confirm_btn').data({"operation": operation, "params": params });
    $('#check_modal').modal('show');
}

$("#modal_confirm_btn").on('click', function() {
    var op_dict = {'save_check' : save_deliverable, 'delete_all_check': delete_all, 'delete_check': del_comment,
                        'publish_check': publish_urls };
    //get the function
    var op = op_dict[$(this).data('operation')];
    var params = $(this).data('params');
    op(...params);
});
