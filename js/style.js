///////////////////Loader////////////////////
$(function () {
    function id(v) { return document.getElementById(v); }
    function loadbar() {
        var ovrl = id("overlay"),
            prog = id("progress"),
            stat = id("progstat"),
            img = document.images,
            c = 0;
        tot = img.length;

        function imgLoaded() {
            c += 1;
            var perc = ((100 / tot * c) << 0) + "%";
            prog.style.width = perc;
            stat.innerHTML = "<img src='images/loader.gif' style='width:80px;' class='loaderImg'/> " + perc;
            if (c === tot) return doneLoading();
        }
        function doneLoading() {
            ovrl.style.opacity = 0;
            setTimeout(function () {
                ovrl.style.display = "none";
            }, 1200);
        }
        for (var i = 0; i < tot; i++) {
            var tImg = new Image();
            tImg.onload = imgLoaded;
            tImg.onerror = imgLoaded;
            tImg.src = img[i].src;
        }
    }
    document.addEventListener('DOMContentLoaded', loadbar, false);
}());
///////////////////...///Loader////////////////////
//////////////For Active tabs from w3School Payalssss /////////////
function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

//////////////...///For Active tabs from w3School Payalssss /////////////

$(document).ready(function () {
   
    ///////For Dropdown select option list
    $(".custom-select").each(function () {
        $(this).wrap("<span class='select-wrapper'></span>");
        $(this).after("<span class='holder'></span>");
    });
    $(".custom-select").change(function () {
        var selectedOption = $(this).find(":selected").text();
        $(this).next(".holder").text(selectedOption);
    }).trigger('change');
    ///////...///For Dropdown select option list

    ///////For Div Move Up Down
    $(".up").click(function () {
        var $current = $(this).closest('div')
        var $previous = $current.prev('div');
        if ($previous.length !== 0) {
            $current.insertBefore($previous);
        }
        return false;
    });

    $(".down").click(function () {
        var $current = $(this).closest('div')
        var $next = $current.next('div');
        if ($next.length !== 0) {
            $current.insertAfter($next);
        }
        return false;
    });
    ///////...///For Div Move Up Down

    ///////...///For Year dropdown list
    for (y = 2000; y <= 2500; y++) {
        var optn = document.createElement("OPTION");
        optn.text = y;
        optn.value = y;

        // if year is 2015 selected
        if (y == new Date().getFullYear()) {
            optn.selected = true;
        }

        document.getElementById('year').options.add(optn);
    }
    ///////...///For Year dropdown list

    /////// For Datepicker
    
    $('#datePicker').datepicker({
        format: 'mm/dd/yyyy',
        autoclose: true,
        toggleActive: true
    })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#eventForm').formValidation('revalidateField', 'date');
        });

    
    //////////...///For Datepicker

    //////////For select Dropdown
    $('.selectpicker').selectpicker({
        style: 'btn-info',
        size: 4,
    });
    //////////...///For select Dropdown
});
//---------------------------------------Prevent anchor default action-------------------------------------------
$('.dropdown-menu').on('click', function (e) {
    if ($(this).hasClass('dropdown-menu')) {
        e.stopPropagation();
    }
});
//---------------------------------------Prevent anchor default action-------------------------------------------
//----------------------------------- switch to dropup only if there is no space at the bottom
$(document).on("shown.bs.dropdown", ".dropdown", function () {
    // calculate the required sizes, spaces
    var $ul = $(this).children(".dropdown-menu");
    var $button = $(this).children(".dropdown-toggle");
    var ulOffset = $ul.offset();
    // how much space would be left on the top if the dropdown opened that direction
    var spaceUp = (ulOffset.top - $button.height() - $ul.height()) - $(window).scrollTop();
    // how much space is left at the bottom
    var spaceDown = $(window).scrollTop() + $(window).height() - (ulOffset.top + $ul.height());
    // switch to dropup only if there is no space at the bottom AND there is space at the top, or there isn't either but it would be still better fit
    if (spaceDown < 0 && (spaceUp >= 0 || spaceUp > spaceDown))
        $(this).addClass("dropup");
}).on("hidden.bs.dropdown", ".dropdown", function () {
    // always reset after close
    $(this).removeClass("dropup");
});

//-------------------------------------- switch to dropup only if there is no space at the bottom

//---------------------dropdowntab-----------------------------
$('#myTabs').on('click', '.nav-tabs a', function () {
    // set a special class on the '.dropdown' element
    $(this).closest('.dropdownP').addClass('dontClose');
})

$('#myDropDown').on('hide.bs.dropdownP', function (e) {
    if ($(this).hasClass('dontClose')) {
        e.preventDefault();
    }
    $(this).removeClass('dontClose');
});

$('.dropdown-menu a[data-toggle="tab"]').click(function (e) {
    e.stopPropagation()
    $(this).tab('show')
});

//---------------------create Content Tab-----------------------------
function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

//// Get the element with id="defaultOpen" and click on it
//document.getElementById("defaultOpen").click();
//---------------------...///create Content Tab-----------------------------
//---------------------create Show hide div in popup-----------------------------
function showDiv() {
    document.getElementById('welcomeDiv').style.display = "block";
    document.getElementById('notimsg').style.display = "none";
}
//---------------------...///create Show hide div in popup-----------------------------
//---------------------Freelancer Popup-----------------------------
// Get the modal
var modal = document.getElementById('mdlAddFreelancer');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
btn.onclick = function () {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
//---------------------...///Freelancer Popup-----------------------------

//--------------------- making current page tab active---------------------------------------
var url = window.location.href.split('inhouse').pop();
$('#bs-example-navbar-collapse-1 li').removeClass("active in");
$('a[href="/inhouse'+ url +'"]').closest('li').addClass('active in')
/// End of tab activation
